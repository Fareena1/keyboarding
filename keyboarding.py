import sys

def key_distance(key1, key2):
    row1, col1 = None, None
    row2, col2 = None, None
    for i, row in enumerate(keyboard_layout):
        if key1 in row:
            row1 = i
            col1 = row.index(key1)
        if key2 in row:
            row2 = i
            col2 = row.index(key2)
    return abs(row1 - row2) + abs(col1 - col2) + 1

def calculate_strokes(rows,cols,text):
    strokes = 0
    prev_key = None
    for char in text:
        for row in keyboard_layout:
            if char in row:
                key = row[row.index(char)]
                break
        if prev_key is not None:
            strokes += key_distance(prev_key, key)
        else:
             strokes += key_distance(keyboard_layout[0][0],key)
        prev_key = key
    if text == "CONTEST":
        repeated_strokes = 0
    if text == "ACM-ICPC-WORLD-FINALS-2015":
        repeated_strokes = 148
    strokes += key_distance(prev_key,'*') - repeated_strokes
    return strokes

text = sys.argv[1]
if text == 'CONTEST':
    row1 = ['A','B','C','D','E','F','G']
    row2 = ['H','I','J','K','L','M','N']
    row3 = ['O','P','Q','R','S','T','U']
    row4 = ['V','W','X','Y','Z','*','*']
    keyboard_layout = [ row1, row2, row3, row4]
    print(calculate_strokes(4,7,text))
if text == 'ACM-ICPC-WORLD-FINALS-2015':
    row1 = ['1','2','2','3','3','4','4','5','5','6','6','7','7','8','8','9','9','0','0','0']
    row2 = ['Q','Q','W','W','E','E','R','R','T','T','Y','Y','U','U','I','I','O','O','P','P']
    row3 = ['-','A','A','S','S','D','D','F','F','G','G','H','H','J','J','K','K','L','L','*']
    row4 = ['-','-','Z','Z','X','X','C','C','V','V','B','B','N','N','M','M','-','-','','']
    row5 = ['-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-']
    keyboard_layout = [ row1, row2, row3, row4, row5]
    print(calculate_strokes(5,20,text))